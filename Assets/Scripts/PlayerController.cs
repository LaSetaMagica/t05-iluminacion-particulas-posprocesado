using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D m_Rigidbody2D;
    public float jumpSpeed;
    public float speed;
    private Rigidbody2D rb;
    //public bool isJumping = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w"))
        {
            Debug.Log("Space");
            transform.Translate(Vector3.up * jumpSpeed * Time.deltaTime, Space.World);
            //rb.velocity = Vector2.up * jumpSpeed;
            //transform.position += Vector3.up * jumpSpeed * Time.deltaTime;
            //if(isJumping == false)
            {
                //rb.AddForce(Vector3.up * jumpSpeed);
                //rb.velocity = Vector2.up * jumpSpeed;
                //isJumping = true;
            }

        }

        if (Input.GetKey("a"))
        {
            Debug.Log("A");
            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        if (Input.GetKey("d"))
        {
            Debug.Log("D");
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
    }

}
